# My common aliases

alias .l="exa --all --long --color=always --group-directories-first"
alias .ll="exa --grid --across --all --color=always --group-directories-first"

alias cat='bat'
alias cls='clear'
alias ec='emacsclient --tty --no-wait'
alias g='git'
alias grep='rg'
alias ll='exa -lah --group-directories-first'
alias zr='source $HOME/.zshrc'
alias webserver='python3 -m http.server 8000'
alias xs=xstarter
